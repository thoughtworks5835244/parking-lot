package org.example;

import org.example.exception.ParkingBoyOperationException;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingManagerTest {

    @Test
    void should_park_and_fetch_car_like_a_standard_parking_boy_when_given_a_parking_manager() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingManager manager = new ParkingManager(List.of(parkingLot1, parkingLot2));

        Ticket ticket1 = manager.park(car);
        assertEquals(car, parkingLot1.fetch(ticket1));

        Ticket ticket2 = manager.park(car);
        assertEquals(car, manager.fetch(ticket2));
    }

    @Test
    void should_park_and_fetch_car_when_park_and_fetch_by_a_parking_boy_given_a_parking_manager() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy1 = new StandardParkingBoy(List.of());
        ParkingBoy parkingBoy2 = new SmartParkingBoy(List.of(parkingLot1));
        ParkingBoy parkingBoy3 = new SuperParkingBoy(List.of(parkingLot2));
        ParkingManager manager = new ParkingManager(List.of(parkingLot1, parkingLot2));
        manager.add(parkingBoy1);
        manager.add(parkingBoy2);
        manager.add(parkingBoy3);

        Ticket ticket1 = manager.parkByBoy(car);
        assertEquals(car, parkingLot1.fetch(ticket1));

        Ticket ticket2 = manager.parkByBoy(car);
        assertEquals(car, manager.fetchByBoy(ticket2));
    }

    @Test
    void should_throw_exception_when_park_and_fetch_by_a_boy_given_a_parking_manager() {
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy1 = new StandardParkingBoy(List.of());
        ParkingManager manager = new ParkingManager(List.of(parkingLot1, parkingLot2));
        manager.add(parkingBoy1);

        Throwable exception1 = assertThrows(
            ParkingBoyOperationException.class,
            () -> manager.fetchByBoy(new Ticket())
        );
        assertEquals("Fetching by boy fail", exception1.getMessage());

        Throwable exception2 = assertThrows(
            ParkingBoyOperationException.class,
            () -> manager.parkByBoy(new Car())
        );
        assertEquals("Parking by boy fail", exception2.getMessage());
    }
}
