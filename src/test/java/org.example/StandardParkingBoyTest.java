package org.example;

import org.example.exception.SpaceUnavailableException;
import org.example.exception.UnrecognizedTicketException;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StandardParkingBoyTest {

    @Test
    void should_park_car_to_first_parking_lot_when_given_2_available_space_parking_lots() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new StandardParkingBoy(List.of(parkingLot1, parkingLot2));

        // when
        Ticket ticket = parkingBoy.park(car);

        // then
        assertEquals(car, parkingLot1.fetch(ticket));
    }

    @Test
    void should_park_car_to_second_parking_lot_when_first_parking_lot_is_full_but_second_parking_lot_has_space() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new StandardParkingBoy(List.of(parkingLot1, parkingLot2));

        // when
        Ticket ticket = parkingBoy.park(car);

        // then
        assertEquals(car, parkingLot2.fetch(ticket));
    }

    @Test
    void should_fetch_right_car_when_given_2_parking_lot_with_1_parked_car_each_and_2_tickets() {
        // given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new StandardParkingBoy(List.of(parkingLot1, parkingLot2));
        Ticket ticket1 = parkingLot1.park(car1);
        Ticket ticket2 = parkingLot2.park(car2);

        // when
        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        Car fetchedCar2 = parkingBoy.fetch(ticket2);

        // then
        assertEquals(car1, fetchedCar1);
        assertEquals(car2, fetchedCar2);
    }

    @Test
    void should_throw_exception_with_correct_message_when_fetch_car_given_a_parking_boy_with_2_parking_lot_and_a_wrong_ticket() {
        // given
        Ticket ticket = new Ticket();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new StandardParkingBoy(List.of(parkingLot1, parkingLot2));

        // when
        Throwable actual = assertThrows(
            UnrecognizedTicketException.class,
            () -> parkingBoy.fetch(ticket)
        );

        // then
        assertEquals("Unrecognized parking ticket.", actual.getMessage());
    }

    @Test
    void should_throw_exception_with_correct_message_when_fetch_car_given_a_parking_boy_with_2_parking_lot_and_a_used_ticket() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new StandardParkingBoy(List.of(parkingLot1, parkingLot2));
        Ticket ticket = parkingBoy.park(car);
        parkingBoy.fetch(ticket);

        // when
        Throwable actual = assertThrows(
            UnrecognizedTicketException.class,
            () -> parkingBoy.fetch(ticket)
        );

        // then
        assertEquals("Unrecognized parking ticket.", actual.getMessage());
    }

    @Test
    void should_throw_exception_with_correct_message_when_park_car_given_a_parking_boy_with_2_full_parking_lot() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new StandardParkingBoy(List.of(parkingLot1, parkingLot2));
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());

        // when
        Throwable actual = assertThrows(
            SpaceUnavailableException.class,
            () -> parkingBoy.park(new Car())
        );

        // then
        assertEquals("No available space.", actual.getMessage());
    }
}
