package org.example;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SuperParkingBoyTest {

    @Test
    void should_park_car_to_the_parking_lot_with_most_space_rate_when_given_2_available_space_parking_lots_and_a_super_parking_boy() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(3);
        ParkingLot parkingLot2 = new ParkingLot(4);
        ParkingBoy parkingBoy = new SuperParkingBoy(List.of(parkingLot1, parkingLot2));
        parkingLot1.park(new Car());
        parkingLot2.park(new Car());

        Ticket ticket = parkingBoy.park(car);

        assertEquals(car, parkingLot2.fetch(ticket));
    }
}
