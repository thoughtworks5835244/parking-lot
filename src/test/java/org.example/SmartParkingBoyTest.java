package org.example;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SmartParkingBoyTest {

    @Test
    void should_park_car_to_the_parking_lot_with_most_space_when_given_2_available_space_parking_lots_and_a_smart_parking_boy() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingBoy parkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));

        Ticket ticket = parkingBoy.park(car);

        assertEquals(car, parkingLot2.fetch(ticket));
    }
}
