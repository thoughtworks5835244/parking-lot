package org.example;

import org.example.exception.SpaceUnavailableException;
import org.example.exception.UnrecognizedTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {

    @Test
    void should_receive_a_ticket_when_park_a_car_given_a_parking_lot_and_a_car() {
        // given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();

        // when
        Ticket ticket = parkingLot.park(car);

        // then
        assertNotNull(ticket);
    }

    @Test
    void should_receive_right_car_when_fetch_a_car_given_a_parking_lot_and_a_ticket() {
        // given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket = parkingLot.park(car);

        // when
        Car fetchedCar = parkingLot.fetch(ticket);

        // then
        assertEquals(car, fetchedCar);
    }

    @Test
    void should_receive_right_car_for_each_ticket_when_fetch_cars_given_a_parking_lot_and_2_tickets() {
        // given
        Car firstCar = new Car();
        Car secondCar = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket firstTicket = parkingLot.park(firstCar);
        Ticket secondTicket = parkingLot.park(secondCar);

        // when
        Car fetchedFirstCar = parkingLot.fetch(firstTicket);
        Car fetchedSecondCar = parkingLot.fetch(secondTicket);

        // then
        assertEquals(firstCar, fetchedFirstCar);
        assertEquals(secondCar, fetchedSecondCar);
    }

    @Test
    void should_throw_exception_with_correct_message_when_fetch_car_given_a_parking_lot_and_a_wrong_ticket() {
        // given
        Ticket ticket = new Ticket();
        ParkingLot parkingLot = new ParkingLot();

        // when
        Throwable actual = assertThrows(
            UnrecognizedTicketException.class,
            () -> parkingLot.fetch(ticket)
        );

        // then
        assertEquals("Unrecognized parking ticket.", actual.getMessage());
    }

    @Test
    void should_throw_exception_with_correct_message_when_fetch_car_given_a_parking_lot_and_a_used_ticket() {
        // given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket = parkingLot.park(car);
        parkingLot.fetch(ticket);

        // when
        Throwable actual = assertThrows(
            UnrecognizedTicketException.class,
            () -> parkingLot.fetch(ticket)
        );

        // then
        assertEquals("Unrecognized parking ticket.", actual.getMessage());
    }

    @Test
    void should_throw_exception_with_correct_message_when_park_car_given_a_full_parking_lot_and_a_car() {
        // given
        ParkingLot parkingLot = new ParkingLot(1);
        parkingLot.park(new Car());

        // when
        Throwable actual = assertThrows(
            SpaceUnavailableException.class,
            () -> parkingLot.park(new Car())
        );

        // then
        assertEquals("No available space.", actual.getMessage());
    }
}
