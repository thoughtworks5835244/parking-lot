package org.example;

import org.example.exception.ParkingBoyOperationException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ParkingManager extends StandardParkingBoy {

    private final List<ParkingBoy> parkingBoyList = new ArrayList<>();

    public ParkingManager(List<ParkingLot> parkingLotList) {
        super(parkingLotList);
    }

    public void add(ParkingBoy boy) {
        if (!parkingBoyList.contains(boy)) {
            parkingBoyList.add(boy);
        }
    }

    public Ticket parkByBoy(Car car) {
        try {
            Optional<ParkingBoy> boy = parkingBoyList.stream()
                .filter(b -> !b.parkingLotList.isEmpty())
                .findFirst();

            if (boy.isPresent()) {
                return boy.get().park(car);
            }
        } catch (Throwable ignore) {
            throw new ParkingBoyOperationException("Parking by boy fail");
        }
        throw new ParkingBoyOperationException("Parking by boy fail");
    }

    public Car fetchByBoy(Ticket ticket) {
        try {
            Optional<ParkingBoy> boy = parkingBoyList.stream()
                .filter(b -> b.parkingLotList.stream().anyMatch(p -> p.isValidTicket(ticket)))
                .findFirst();

            if (boy.isPresent()) {
                return boy.get().fetch(ticket);
            }
        } catch (Throwable ignore) {
            throw new ParkingBoyOperationException("Fetching by boy fail");
        }
        throw new ParkingBoyOperationException("Fetching by boy fail");
    }
}
