package org.example;

import java.util.List;
import java.util.Optional;

public class StandardParkingBoy extends ParkingBoy {

    public StandardParkingBoy(List<ParkingLot> parkingLotList) {
        super(parkingLotList);
    }

    protected Optional<ParkingLot> findAvailableParkingLot() {
        return this.parkingLotList.stream()
            .filter(p -> !p.isFull())
            .findFirst();
    }
}
