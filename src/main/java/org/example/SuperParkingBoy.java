package org.example;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class SuperParkingBoy extends ParkingBoy {

    public SuperParkingBoy(List<ParkingLot> parkingLotList) {
        super(parkingLotList);
    }

    protected Optional<ParkingLot> findAvailableParkingLot() {
        return this.parkingLotList.stream()
            .max(Comparator.comparingDouble(ParkingLot::getAvailableSpaceRate));
    }
}
