package org.example;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class SmartParkingBoy extends ParkingBoy {

    public SmartParkingBoy(List<ParkingLot> parkingLotList) {
        super(parkingLotList);
    }

    protected Optional<ParkingLot> findAvailableParkingLot() {
        return this.parkingLotList.stream()
            .max(Comparator.comparingInt(ParkingLot::getAvailableSpace));
    }
}
