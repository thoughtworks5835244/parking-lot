package org.example.exception;

public class SpaceUnavailableException extends RuntimeException {

    public SpaceUnavailableException() {
        super("No available space.");
    }
}
