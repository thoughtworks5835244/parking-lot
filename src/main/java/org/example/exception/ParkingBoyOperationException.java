package org.example.exception;

public class ParkingBoyOperationException extends RuntimeException {

    public ParkingBoyOperationException(String message) {
        super(message);
    }
}
