package org.example;

import org.example.exception.SpaceUnavailableException;
import org.example.exception.UnrecognizedTicketException;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {

    private static final int DEFAULT_CAPACITY = 10;

    private final int capacity;
    private final Map<Ticket, Car> records = new HashMap<>();

    public ParkingLot() {
        this.capacity = DEFAULT_CAPACITY;
    }

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public Ticket park(Car car) {
        if (isFull()) {
            throw new SpaceUnavailableException();
        }

        Ticket ticket = new Ticket();
        records.put(ticket, car);
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        if (!isValidTicket(ticket)) {
            throw new UnrecognizedTicketException();
        }
        return records.remove(ticket);
    }

    public boolean isFull() {
        return records.size() >= capacity;
    }

    public boolean isValidTicket(Ticket ticket) {
        return records.containsKey(ticket);
    }

    public int getAvailableSpace() {
        return capacity - records.size();
    }

    public double getAvailableSpaceRate() {
        return (double) getAvailableSpace() / capacity;
    }
}
