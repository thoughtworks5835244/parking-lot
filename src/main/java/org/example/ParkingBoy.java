package org.example;

import org.example.exception.SpaceUnavailableException;
import org.example.exception.UnrecognizedTicketException;

import java.util.List;
import java.util.Optional;

public abstract class ParkingBoy {

    protected final List<ParkingLot> parkingLotList;

    public ParkingBoy(List<ParkingLot> parkingLotList) {
        this.parkingLotList = parkingLotList;
    }

    protected abstract Optional<ParkingLot> findAvailableParkingLot();

    public Ticket park(Car car) {
        Optional<ParkingLot> parkingLot = findAvailableParkingLot();

        if (parkingLot.isPresent()) {
            return parkingLot.get().park(car);
        }

        throw new SpaceUnavailableException();
    }

    public Car fetch(Ticket ticket) {
        Optional<ParkingLot> parkingLot = parkingLotList.stream()
            .filter(p -> p.isValidTicket(ticket))
            .findFirst();

        if (parkingLot.isPresent()) {
            return parkingLot.get().fetch(ticket);
        }

        throw new UnrecognizedTicketException();
    }
}
